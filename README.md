# How to use it
1. Clone this repository in the manim-master/active_projects folder. In case you want to locate it in another place, you must replace the import lines:
```python
# debugging_formulas.py
from active_projects.chemical_process.utils import get_formula_numbers
# scene1.py
from active_projects.chemical_process.debugging_formulas import Engschrift,formulas,ChemProcAbs
# scene2.py
from active_projects.chemical_process.scene1 import *
from active_projects.chemical_process.debugging_formulas import formulas
```

2. Replace the manimlib/tex_template.tex file with the .tex file from this repository, or the following packages should be included in the manimlib/tex_template.tex file:
```latex
\usepackage{chemfig}
\usepackage[version=4]{mhchem}
```
3. Install the font.ttf, if it is already installed, you must edit the name in the file debugging_formulas.py:
```python
# chemical_process/debugging_formulas.py Lines 4 -> 9
class Engschrift(Text):
    CONFIG = {
        "stroke_width": 0,
        "height": 3,
        "font": "DIN Engschrift Std" # <- NAME OF YOUR FONT
    }
```

# Small guide:
* The abstract class `ChemProcAbs` is defined in debbuging_formulas.py, there you can see all the changes you can make in the CONFIG dictionary
* You can modify the color of the elements, indexes, links, etc. You just have to be specific in the CONFIG dictionary.