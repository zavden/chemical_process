from manimlib.imports import *
from active_projects.chemical_process.utils import get_formula_numbers

class Engschrift(Text):
    CONFIG = {
        "stroke_width": 0,
        "height": 3,
        "font": "DIN Engschrift Std" # <- NAME OF YOUR FONT
    }

class TestingText(Scene):
    def construct(self):
        text = Engschrift("Hello world")
        self.add(text)

def Range(x,y=None):
    if y == None:
        return list(range(x))
    else:
        return list(range(x,y))

formulas = {
    # FORMULA 1
    "1": {
        "tex": r"""
                \schemestart
                \chemfig[atom sep=20pt]{CH_3-CH_2-CH_2-CH_2-CH_2-CH_2-CH_2-CH_3}
                \+
                \chemfig[atom sep=20pt]{O=O}
                \arrow(.mid east--.mid west)
                \chemfig[atom sep=20pt]{O=C=O}\+
                \chemfig[atom sep=20pt]{H-[:60]O-[:-60]H}
                \schemestop
        """,
        "reactant": {    
            "molecule": {
                    "1": {
                        "C": [0,3,7,11,15,19,23,27],
                        "H": [1,4,8,12,16,20,24,28],
                        "bonds": [6,10,14,18,22,26,30],
                        "index": {
                            "H": [2,5,9,13,17,21,25,29]
                        },
                        "full": Range(31)
                    },
                    "2": {
                        "O": [32,33],
                        "bonds": [34,35],
                        "full": Range(32,36)
                    },
            },
            "sums": [31],
            "full": list(range(34))
        },
        "product": {    
            "molecule": {
                    "1": {
                        "C": [41],
                        "O": [40,44],
                        "bonds": [42,43,45,46],
                        "full": Range(40,47)
                    },
                    "2": {
                        "O": [49],
                        "H": [48,51],
                        "bonds": [50,52],
                        "full": Range(48,53)
                    }
            },
            "sums": [47],
            "full": Range(40,52)
        },
        "arrow": (37,40)
    },
    # FORMULA 2
    "2":{
        "tex": r"""
                    \schemestart
                    \chemfig{C_8H_{18}}
                    \+
                    \chemfig{O_2}
                    \arrow(.mid east--.mid west)
                    \chemfig{CO_2}
                    \+
                    \chemfig{H_2O}
                    \schemestop
        """,
        "reactant": {    
            "molecule": {
                    "1": {
                        "C": [0],
                        "H": [2],
                        "index": {
                            "C": [1],
                            "H": [3,4]
                        },
                        "full": Range(5)
                    },
                    "2": {
                        "O": [6],
                        "index": {
                            "O": [7]
                        },
                        "full": Range(6,8)
                    }
            },
            "sums": [5],
            "full": Range(8)
        },
        "product": {    
            "molecule": {
                    "1": {
                        "C": [12],
                        "O": [13],
                        "index": {
                            "O": [14]
                        },
                        "full": Range(12,15)
                    },
                    "2": {
                        "O": [18],
                        "H": [16],
                        "index": {
                            "H": [17]
                        },
                        "full": Range(16,19)
                    }
            },
            "sums": [15],
            "full": Range(12,19)
        },
        "arrow": (9,12)
    }
}

class FormulasDebbug(Scene):
    def construct(self):
        self.formulas = formulas
        formulas_t = VGroup(*[
            TextMobject(formulas[f"{i+1}"]["tex"],stroke_width=0.5)[0].set_width(FRAME_WIDTH-1)
            for i in range(2)
        ])
        formulas_t.arrange(DOWN)
        for i_formula in range(len(formulas_t)):
            print(f"Formula {i_formula+1}: ",len(formulas_t[i_formula])," elements")
        get_formula_numbers(*formulas_t)
        self.add(
            formulas_t,
            *[formulas_t[i].t_numbers for i in range(len(formulas_t))],
        )
#Structure
# formula {1,2} ->  {reactant,product,arrow}
#                   {reactant,product} -> {molecule,sums}
#                                         molecule -> {1,2}
#                                                     {1,2} -> {C,O,H,index,bonds}
#                                                              index -> {C,H,O} 
class ChemProcAbs(Scene):
    CONFIG = {
        "formulas_buff":2,
        "width_bonds":2.5,
        "font_size":2.5,
        "colors": {
            "formula": {
                # FORMULA 1
                "1": {
                    "reactant": {    
                        "molecule": {
                                "1": {
                                    "C": WHITE,
                                    "H": WHITE,
                                    "bonds": WHITE,
                                    "index": {
                                        "H": WHITE
                                    }
                                },
                                "2": {
                                    "O": WHITE,
                                    "bonds": WHITE,
                                }
                        },
                        "sums": WHITE
                    },
                    "product": {    
                        "molecule": {
                                "1": {
                                    "C": WHITE,
                                    "O": WHITE,
                                    #"bonds": GREEN_A
                                },
                                "2": {
                                    "O": WHITE,
                                    "H": WHITE,
                                    "bonds": WHITE
                                }
                        },
                        "sums": WHITE
                    },
                    "arrow": WHITE
                },
                # FORMULA 2
                "2":{
                    "reactant": {    
                        "molecule": {
                                "1": {
                                    "C": WHITE,
                                    "H": WHITE,
                                    "index": {
                                        "C": WHITE,
                                        "H": WHITE
                                    }
                                },
                                "2": {
                                    "O": WHITE,
                                    "index": {
                                        "O": WHITE
                                    }
                                }
                        },
                        "sums": WHITE
                    },
                    "product": {    
                        "molecule": {
                                "1": {
                                    "C": WHITE,
                                    "O": WHITE,
                                    "index": {
                                        "O": WHITE
                                    }
                                },
                                "2": {
                                    "O": WHITE,
                                    "H": WHITE,
                                    "index": {
                                        "H": WHITE
                                    }
                                },    
                        },
                        "sums": WHITE
                    },
                    "arrow": WHITE
                }
            }
        }
    }
    def setup(self):
        self.removable_mobjects = VGroup()
        formulas_t = VGroup(*[
            TextMobject(formulas[f"{i+1}"]["tex"],stroke_width=0.5,background_stroke_width=0)[0]
            for i in range(2)
        ])
        formulas_t[0].set_width(FRAME_WIDTH-1)
        formulas_t.arrange(DOWN,buff=self.formulas_buff)
        c = self.colors
        #Structure
        # formula {1,2} ->  {reactant,product,arrow}
        #                   {reactant,product} -> {molecule,sums}
        #                                         molecule -> {1,2}
        #                                                     {1,2} -> {C,O,H,index,bonds}
        #                                                              index -> {C,H,O} 
        # Change color arrows
        arrows = VGroup(*[
                Arrow(
                    formulas_t[i][
                        formulas[f"{i+1}"]["arrow"][0]:formulas[f"{i+1}"]["arrow"][1]
                    ].get_left(),
                    formulas_t[i][
                        formulas[f"{i+1}"]["arrow"][0]:formulas[f"{i+1}"]["arrow"][1]
                    ].get_right(),
                    buff=0,
                    background_stroke_opacity=0,
                )
                for i in range(len(formulas_t))
            ])
        for arrow in arrows:
            arrow[-1].shift(LEFT*0.05)
        # Remove the LaTeX arrows
        for i in range(len(formulas_t)):
            formulas_t[i][
                        formulas[f"{i+1}"]["arrow"][0]:formulas[f"{i+1}"]["arrow"][1]
                    ].fade(1)
        # Change color rest
        if "formula" in c:
            for i_formula in range(len(formulas_t)):
                # formula[i_formula]
                for i_sub_formula in formulas[f"{i_formula+1}"].keys():
                    #formula[i_formula][i_subformula]
                    #   i_sub_formula != tex, arrow
                    for type_molecule in ["product","reactant"]:
                        for num_molecules in formulas[f"{i_formula+1}"][type_molecule]["molecule"].keys():
                            if "bonds" in formulas[f"{i_formula+1}"][type_molecule]["molecule"][num_molecules]:
                                for i_symbol in formulas[f"{i_formula+1}"][type_molecule]["molecule"][num_molecules]["bonds"]:
                                    if self.width_bonds != None:
                                        formulas_t[i_formula][i_symbol].set_stroke(width=self.width_bonds)
                                    else:
                                        formulas_t[i_formula][i_symbol].set_stroke(width=2.5)
                    if i_sub_formula not in ["tex","arrow"] and f"{i_formula+1}" in c["formula"]:
                        if "molecule" in c["formula"][f"{i_formula+1}"][i_sub_formula]:
                            for molecule in formulas[f"{i_formula+1}"][i_sub_formula]["molecule"].keys():
                                #formula[i_formula][i_subformula]["molecule"][molecule]
                                if molecule in c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"]:
                                    for element in formulas[f"{i_formula+1}"][i_sub_formula]["molecule"][molecule].keys():
                                        #formula[i_formula][i_subformula]["molecule"][molecule][element]
                                        if element in c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"][molecule]:
                                            if element not in ["index","full"]:
                                                for i_symbol in formulas[f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element]:
                                                    # Existence of element in colors
                                                    if element in c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"][molecule].keys():
                                                        formulas_t[i_formula][i_symbol].set_color(
                                                                c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element]
                                                        )
                                            if element == "index":
                                                for sub_element in formulas[f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element]:
                                                    for i_symbol in formulas[f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element][sub_element]:
                                                        # Existence of element in colors
                                                        if sub_element in c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element].keys():
                                                            formulas_t[i_formula][i_symbol].set_color(
                                                                    c["formula"][f"{i_formula+1}"][i_sub_formula]["molecule"][molecule][element][sub_element]
                                                            )
                        if "sums" in c["formula"][f"{i_formula+1}"][i_sub_formula]:
                            for i_sum in formulas[f"{i_formula+1}"][i_sub_formula]["sums"]:
                                formulas_t[i_formula][i_sum].set_color(
                                        c["formula"][f"{i_formula+1}"][i_sub_formula]["sums"]
                                )
        # FORMULA NAMES
        names = VGroup(*[
            VGroup(
                *[Engschrift(t,height=self.font_size) for t in p]
            )
            for p in [["oktan","dioxygen"],["carbondioxid","vand"]]
        ])
        all_names = VGroup(*[names.copy() for _ in range(2)])
        aling_dots = []
        for i_formula in range(2):
            for i_sub_formula,i_sub_ind in zip(["reactant","product"],range(2)):
                for molecule in range(2):
                    rf = formulas[f"{i_formula+1}"][i_sub_formula]["molecule"][f"{molecule+1}"]["full"]
                    dot = formulas_t[i_formula][rf[0]:rf[-1]].get_center()
                    all_names[i_formula][i_sub_ind][molecule].next_to(
                        formulas_t[i_formula][rf[0]:rf[-1]].get_center(),
                        DOWN,
                        buff=0.4,
                        aligned_edge=UP
                    )
                    #self.add(Dot(dot))  #<---- REMOVE
            dot = Dot().next_to(formulas_t[i_formula],DOWN,buff=0.3)
            aling_dots.append(
                 dot.get_center()
                )
        # ALIGN FORMULA NAMES
        for n1,coord in zip(all_names,aling_dots):
            n1[0][1].scale(1.2)
            for n2 in n1:
                for n3 in n2:
                    n3.set_y(coord[1])

        self.formulas_t = formulas_t
        self.arrows_t = arrows
        self.all_names = VGroup(*[
            mob
            for sub_sub_names in all_names
            for sub_names in sub_sub_names
            for mob in sub_names
        ])
        self.formulas_group = VGroup(*[VGroup(f,a) for f,a in zip(self.formulas_t,self.arrows_t)])