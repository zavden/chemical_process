from active_projects.chemical_process.scene1 import *
from active_projects.chemical_process.debugging_formulas import formulas

#Structure
# formula {1,2} ->  {reactant,product,arrow}
#                   {reactant,product} -> {molecule,sums}
#                                         molecule -> {1,2}
#                                                     {1,2} -> {C,O,H,index,bonds}
#                                                              index -> {C,H,O} 
class Scene2(Scene1):
    def construct(self):
        formulas_t,arrows_t,all_names = self.formulas_t,self.arrows_t,self.all_names
        self.add(formulas_t[0][0:36],all_names[0][0])
        self.play(GrowArrow(arrows_t[0]))
        # Transform carbons formula 1:
        self.play(
            ReplacementTransform(
                self.get_formulas(0,"reactant",1,"C"),
                self.get_formulas(0,"product",1,"C")
            ),
            Write(
                self.get_formulas(0,"product",1,"bonds")
            ),
            path_arc=PI/2,
            run_time=4
        )
        self.play(
            ReplacementTransform(
                self.get_formulas(0,"reactant",2,"O"),
                self.get_formulas(0,"product",1,"O")
            ),
            ReplacementTransform(
                self.get_formulas(0,"reactant",2,"O"),
                self.get_formulas(0,"product",2,"O")
            ),
            Write(
                self.get_formulas(0,"product",2,"bonds")
            ),
            path_arc=PI/2,
            run_time=4
        )
        self.play(
            ReplacementTransform(
                self.get_formulas(0,"reactant",1,"H"),
                self.get_formulas(0,"product",2,"H")
            ),
            Write(self.get_sums(0,"product","sums")),
            path_arc=PI/2,
            run_time=4
        )
        self.play(Write(all_names[0][1]))
        self.wait()

        # FORMULA 2
        self.play(
            Write(formulas_t[1][0:9]),
            Write(all_names[1][0]),
            GrowArrow(arrows_t[1])
        )
        # An1
        self.play(
            ReplacementTransform(
                self.get_formulas(1,"reactant",1,"C"),
                self.get_formulas(1,"product",1,"C"),
            ),
            ReplacementTransform(
                self.get_formulas(1,"reactant",2,"O"),
                self.get_formulas(1,"product",1,"O"),
            ),
            ReplacementTransform(
                self.get_formulas_index(1,"reactant",2,"O"),
                self.get_formulas_index(1,"product",1,"O"),
            ),
            path_arc=PI/2,
            run_time=4
        )
        # An2
        self.play(
            ReplacementTransform(
                self.get_formulas(1,"reactant",1,"H"),
                self.get_formulas(1,"product",2,"H"),
            ),
            ReplacementTransform(
                self.get_formulas(1,"reactant",2,"O"),
                self.get_formulas(1,"product",2,"O"),
            ),
            Write(self.get_sums(1,"product","sums")),
            Write(self.get_formulas_index(1,"product",2,"H")),
            path_arc=PI/2,
            run_time=4
        )
        self.play(Write(all_names[1][1]))
        self.wait()

    def get_formulas(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"][el]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret

    def get_sums(self,f,sub_f,type_):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f][type_]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret

    def get_formulas_index(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"]["index"][el]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret