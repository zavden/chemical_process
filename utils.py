from manimlib.imports import *

def get_formula_numbers(
                 *formulas,
                 f_kwargs={"height":1,"font":"Arial","stroke_width":0,"color":RED},
                 nt_kwargs={"direction":UP,"buff":0}
                 ):
    for formula in formulas:
        n = 0
        formula.t_numbers = VGroup()
        for f in formula:
            t = Text(f"{n}",**f_kwargs)
            t.next_to(f,**nt_kwargs)
            formula.t_numbers.add(t)
            n += 1
