from manimlib.imports import *
from active_projects.chemical_process.debugging_formulas import Engschrift,formulas,ChemProcAbs

CARBON_COLOR = RED
OXYGEN_COLOR = BLUE
HYDROGEN_COLOR = YELLOW

class Scene1(ChemProcAbs):
    CONFIG = {
            "pauses":{
            # Molecules
                "m1": 0.5, #m1 <- molecule 1
                "n1": 0.5, #n1 <- name 1 
                "m2": 0.5,
                "n2": 0.5,
                "m3": 0.5,
                "n3": 0.5,
                "m4": 0.5,
                "n4": 0.5,
            },
            "colors": {
            "formula": {
                # FORMULA 1
                "1": {
                    "reactant": {    
                        "molecule": {
                                "1": {
                                    "C": CARBON_COLOR,
                                    "H": HYDROGEN_COLOR,
                                    "index": {
                                        "H": HYDROGEN_COLOR
                                    }
                                },
                                "2": {
                                    "O": OXYGEN_COLOR,
                                }
                        },
                    },
                    "product": {    
                        "molecule": {
                                "1": {
                                    "C": CARBON_COLOR,
                                    "O": OXYGEN_COLOR,
                                },
                                "2": {
                                    "O": OXYGEN_COLOR,
                                    "H": HYDROGEN_COLOR,
                                }
                        },
                    },
                },
                # FORMULA 2
                "2":{
                    "reactant": {    
                        "molecule": {
                                "1": {
                                    "C": CARBON_COLOR,
                                    "H": HYDROGEN_COLOR,
                                    "index": {
                                        "C": CARBON_COLOR,
                                        "H": HYDROGEN_COLOR
                                    }
                                },
                                "2": {
                                    "O": OXYGEN_COLOR,
                                    "index": {
                                        "O": OXYGEN_COLOR
                                    }
                                }
                        },
                    },
                    "product": {    
                        "molecule": {
                                "1": {
                                    "C": CARBON_COLOR,
                                    "O": OXYGEN_COLOR,
                                    "index": {
                                        "O": OXYGEN_COLOR
                                    }
                                },
                                "2": {
                                    "O": OXYGEN_COLOR,
                                    "H": HYDROGEN_COLOR,
                                    "index": {
                                        "H": HYDROGEN_COLOR
                                    }
                                },    
                        },
                    },
                }
            }
        }
    }
#Structure
# formula {1,2} ->  {reactant,product,arrow}
#                   {reactant,product} -> {molecule,sums}
#                                         molecule -> {1,2}
#                                                     {1,2} -> {C,O,H,index,bonds}
#                                                              index -> {C,H,O} 
    def construct(self):
        formulas_t,arrows_t,all_names = self.formulas_t,self.arrows_t,self.all_names

        formulas_t_without_sums = formulas_t.copy()

        # Get sum sysmbols
        formulas_t_sums_pre = VGroup(*[
            self.get_sums_from_entire_formula(f,sub_f)
            for sub_f in ["reactant","product"]
            for f in range(2)
        ]).copy()
        molecules = VGroup(*[
            self.get_molecule(f,sub_f,mol)
            for f in range(2)
            for sub_f in ["reactant","product"]
            for mol in range(2)
        ])
        #  molecule
        def show_molecule(i):
            i_s = i%4
            self.play(Write(molecules[i]))
            self.wait(self.pauses[f"n{i_s+1}"])
            self.play(Write(all_names[i]))
            self.wait(self.pauses[f"m{i_s+1}"])

        # little details (align second formula "vand" name)
        all_names[-1].shift(RIGHT*0.2)
        # Fist formula
        # Show okten
        show_molecule(0)
        # Show sum symbol
        self.play(FadeIn(formulas_t_sums_pre[0]))
        # show oxygen
        show_molecule(1)
        # show arrow
        self.play(GrowArrow(arrows_t[0]))
        # show carbondioxid
        show_molecule(2)
        # show sum symbol
        self.play(FadeIn(formulas_t_sums_pre[2]))
        # show vand
        show_molecule(3)
        self.wait()
        # Second formula
        show_molecule(4)
        self.play(FadeIn(formulas_t_sums_pre[1]))
        show_molecule(5)
        self.play(GrowArrow(arrows_t[1]))
        show_molecule(6)
        self.play(FadeIn(formulas_t_sums_pre[3]))
        show_molecule(7)
        self.wait()

        # self.play(
        #     *list(map(Write,formulas_t_without_sums)),
        #     LaggedStartMap(FadeIn,formulas_t_sums),
        #     *list(map(Write,all_names)),
        #     *list(map(GrowArrow,arrows_t))
        # )
        self.wait()

    # GET FORMULAS WITHOUT COPY
    def get_formulas(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"][el]
        ])
        return ret

    def get_sums_from_entire_formula(self,f,sub_f):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["sums"]
        ])
        return ret

    def get_molecule(self,f,sub_f,mol):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol+1}"]["full"]
        ])
        return ret

    def get_formulas_index(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"]["index"][el]
        ])
        return ret

    # GET FORMULAS FROM COPY
    def get_formulas_copy(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"][el]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret

    def get_sums_copy(self,f,sub_f,type_):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f][type_]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret

    def get_formulas_index_copy(self,f,sub_f,mol,el):
        ret = VGroup(*[
            self.formulas_t[f][i]
            for i in formulas[f"{f+1}"][sub_f]["molecule"][f"{mol}"]["index"][el]
        ]).copy()
        self.removable_mobjects.add(ret)
        return ret